"""blogMixin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
# from django.conf import urls
from django.urls import path, re_path
from blog import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', views.login),
    path('valid_code/', views.get_valid_code),
    path('index/', views.index),
    re_path(r'^$', views.index),
    # re_path(r'^(?P<username>\w+)$', views.home_site),
    re_path(r'^(?P<username>\w+)/(?P<condition>tag|category|archive)/(?P<params>.*)/$', views.home_site),
    path('logout', views.login),
    path('register/', views.register),
    re_path('^(?P<username>\w+)/article_detail/(?P<article_id>\d+)$', views.article_detail),
    path('digg/', views.digg),
    path('comment/', views.comment),
    path('comment_tree/', views.get_comment_tree)
]
