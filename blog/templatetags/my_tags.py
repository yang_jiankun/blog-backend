from django import template
from django.db.models import Count
from blog.models import UserInfo, Category, Article,Tag
from blog import models
register = template.Library()

@register.simple_tag
def multi_tag(x, y):
  return x*y


@register.inclusion_tag('classification.html')
def get_classification_style(username):
  user = UserInfo.objects.filter(username=username).first()
  blog = user.blog
  cate_list = models.Category.objects.filter(blog=blog).values('pk').annotate(c=Count('article__title')).values('title', "c")
  # 查询当前站点每一个标签对应的文章数
  tag_list = models.Tag.objects.filter(blog=blog).values('pk').annotate(c=Count('article')).values_list('title', "c")
  # 查询当前站点每一个年月的名称和对应的文章数
  data_list = models.Article.objects.filter(user=user).extra(select={'y_m_d_date': "date_format(create_time,'%%Y-%%m')"}).values("y_m_d_date").annotate(c=Count('nid')).values('y_m_d_date', 'c')
  return { 'cate_list': cate_list, 'tag_list': tag_list, 'data_list': data_list}
